import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {createPost} from '../actions';

class PostsNew extends Component{

    renderField(field){
        const {meta : {touched, error}} = field; //means field.meta since keys are names the same
        const className = `form-group ${touched && error? 'has-danger' : ''}`;
        return(
            <div className={className}>
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type="text"
                    {...field.input}
                />
                <div className="text-help">
                {touched ? error : ''}
                </div>
            </div>
        );
    }

    onSubmit(values){
        this.props.createPost(values, () => {
            this.props.history.push('/');
        });
    }

    render(){
        const {handleSubmit} = this.props;
        return (
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <Field name="title" label="Title"
                  component={this.renderField}/>
              <Field name="categories"label="Categories"
                  component={this.renderField}/>
              <Field name="content"label="Post Content"
                     component={this.renderField}/>
              <button type="submit" className="btn btn-primary">Submit</button>
              <Link className="btn btn-danger" to="/">Cancel</Link>
          </form>
        );
    }
}

/**
 * This will be used bu the form helper
 * @param values the form values to validate in json form:
 * {
 *   title: mytitle,
 *   categories: ctage,
 *   content: content
 * }
 *
 * @return an array of errors, if empty redux-form assumes all is good
 */
function validate(values){
    const errors = {};

    if(!values.title){
        errors.title = "Enter a title!";
    }
    if(!values.categories){
        errors.categories = "Enter at least one category!";
    }
    if(!values.content){
        errors.content = "Post content cannot be empty!";
    }

    return errors;
}

export default reduxForm({
    form:'PostsNewForm',
    validate
})(connect(null, {createPost})(PostsNew));