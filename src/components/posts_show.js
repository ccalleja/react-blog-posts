import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchPost, deletePost} from '../actions';
import {Link} from 'react-router-dom';

class PostsShow extends Component {

    componentDidMount() {

        //if we dont want to fect the post if we already have in in state
        if (!this.props.post) {
            //provded by react-router, id will map to :id defined in the route config
            //OLD ES5 WAY
            //const id = this.props.match.params.id;
            //ES6
            const {id} = this.props.match.params;
            this.props.fetchPost(id);
        }
    }

    onDeleteClick(){
        const {id} = this.props.match.params;
        //we can also access id from props.post however sometimes it can be empty on first load
        this.props.deletePost(id, () => {
            this.props.history.push('/');
        });
    }

    render() {
        const {post} = this.props;

        //REMEMBER !!! - our component first renders before the componentDidMount executes
        if (!post) {
            return <div>Loading...</div>
        }
        return (
            <div>
                <h3>{post.title}</h3>
                <h6>Categories: {post.categories}</h6>
                <p>{post.content}</p>
                <Link to="/" className="btn btn-primary">Back to index</Link>
                <button className="btn btn-danger pull-xs-right"
                        onClick={this.onDeleteClick.bind(this)}>
                    Delete Post
                </button>
            </div>
        );
    }
}

/**
 * @param posts, the application state props
 * @param ownProps, a reference to the component props
 */
function mapStateToProps({posts}, ownProps) {
    //this returns all posts and then we need to filter in the render
    //return {posts};

    //using ownProps we can filter the list here
    return {post: posts[ownProps.match.params.id]};
}

export default connect(mapStateToProps, {fetchPost, deletePost})(PostsShow);