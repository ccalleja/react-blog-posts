import axios from 'axios';

//GLOBAL
export const FETCH_POSTS = 'fetch_posts';
export const FETCH_POST = 'fetch_post';
export const DELETE_POST = 'delete_post';
export const CREATE_POST = 'create_post';
//LOCAL
const ROOT_URL = 'http://reduxblog.herokuapp.com/api';
const API_KEY = '?key=ccalleja12345';

export function fetchPosts() {
    const request = axios.get(`${ROOT_URL}/posts${API_KEY}`);
    return {
        type: FETCH_POSTS,
        payload: request
    };
}

export function createPost(values, callback){
    const request = axios.post(`${ROOT_URL}/posts${API_KEY}`, values)
        .then(() => callback());
    return (disptach) => {
        request.then((data) => {
            disptach({
                type: CREATE_POST,
                payload: data
            });
        });
    };
}

export function fetchPost(id){
    const request = axios.get(`${ROOT_URL}/posts/${id}${API_KEY}`);
    return (disptach) => {
        request.then((data) => {
           disptach({
               type: FETCH_POST,
               payload: data
           });
        });
    };
}
export function deletePost(id, callback){
    const request = axios.delete(`${ROOT_URL}/posts/${id}${API_KEY}`)
        .then(() => callback());
    return (disptach) => {
        request.then((data) => {
            disptach({
                type: DELETE_POST,
                payload: data.id
            });
        });
    };
}