import {FETCH_POSTS, FETCH_POST, DELETE_POST} from '../actions';
import _ from 'lodash';

export default function (state = {}, action) {
    switch (action.type) {
        case FETCH_POST:
            const post = action.payload.data;

            //trditional ES5 approach
            /*const newState = {...state};
            newState[post.id] = post;
            return newState;*/

            //ES6
            return {...state, [post.id] : post};

        case FETCH_POSTS:
            return _.mapKeys(action.payload.data, 'id');

        case DELETE_POST:
            //payload here is the id of the deleted post
            return _.omit(state, action.payload);

        default:
            return state;
    }
}
