import { combineReducers } from 'redux';
//importing a reducer from a dependency so to interact with its state
import {reducer as formReducer} from 'redux-form';
import PostsReducer from './reducer_posts';


/**
 * formReducer is needed by redux-form to work
 * @type {Reducer<S>}
 */
const rootReducer = combineReducers({
    posts: PostsReducer,
    form: formReducer
});

export default rootReducer;
