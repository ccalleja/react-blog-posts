# Blog Posts App 

Cloned from [ReduxSimpleStarter ](https://github.com/StephenGrider/ReactStarter/releases) following the course [React-Redux](https://www.udemy.com/react-redux/)

A simple application allowing the user to do crud operations and navigate trough the application to see all posts, show a post, delete a post and create posts

### How to run
```
> npm install
> npm start
```